module.exports= {
    db:{
        dbPath: process.env.MONGO_PATH || "mongodb://127.0.0.1:27017/lazyrestapi",
        db_options:{
            user:'lazyadmin',
            pass:'admin123'
        }
    },
    port: 9227,
    timeZone: "Asia/Jakarta",
    secretKey:"iniSecretKeyOfSecretKey"
};
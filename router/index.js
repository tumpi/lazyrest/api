/**
 * Created by muharizals on 17/11/2017.
 */

const UserController =require('../controller/UsersController');
const ProjectController =require('../controller/ProjectController');
let errors = require('restify-errors');
const jwt = require('restify-jwt');
const {secretKey} = require('../core/config');

class Router{
    configure(server){

        server.use(
            jwt({secret:secretKey})
                .unless({path: [
                    '/',
                    '/login'
                ]})
        );


        server.get('/',(req,res)=>{
            res.send({
                name:'LazyRestApi',
                author:'Loopin',
                description:'lazy rest api for lazy people'
            })
        });

        server.post('/login',UserController.login);

        server.post('/project',ProjectController.create);
        server.del('/project/:id',ProjectController.del);
        server.put('/project/:id',ProjectController.update);
        server.get('/project',ProjectController.list);
        server.get('/project/:id',ProjectController.get);

    }
}

module.exports = new Router();
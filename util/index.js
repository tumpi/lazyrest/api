/**
 * Created by muharizals on 18/11/2017.
 */

const jwtoken = require('jsonwebtoken');
const {secretKey} = require('../core/config');
const randomString = require('randomstring');

class MyUtil{
    isEmpty(obj){
        for(let prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

    getFromName(name="lazy"){
        return name.substring(0,3).toLowerCase();
    }

    generateSecretKey(){
        return randomString.generate({length:20})
    }

    genereateAccesToken(){
        return  `Lz::${randomString.generate({length:40})}?`
    }

    getAccessToken(user){
        return jwtoken.sign(user,secretKey);
    }

    getOptions(req){
        let options = {
            sort:'-createdAt',
            limit:100,
            skip:0,
            page:1
        };
        if(req.params.hasOwnProperty('offset') && !isNaN(req.params.offset)){
            options.skip = parseInt(req.params.offset);
            options.page = parseInt((req.params.offset / 20) + 1);
            if(options.page <= 0){
                options.page = 1;
            }
        }
        if(req.params.hasOwnProperty('limit') && !isNaN(req.params.limit)){
            options.limit = parseInt(req.params.limit);
        }
        if(req.params.hasOwnProperty('sort')){
            options.sort = req.params.sort;
        }
        if(req.params.hasOwnProperty('limit') && req.params.limit == 0){
            throw {
                meta:{
                    current_page:options.page,
                    total_data:0,
                    limit:options.limit,
                    sort:options.sort,
                    offset:options.offset
                },
                data:[]
            };

        }
        if(req.params.hasOwnProperty('page') && !isNaN(req.params.page)){
            options.skip = parseInt((req.params.page * 20) - 20 );
            options.page = parseInt(req.params.page);
            options.limit = 20
        }
        if(options.skip < 0){
            options.skip = 0;
            options.page = 1;
        }
        return options;
        
    }
}

module.exports = new MyUtil();
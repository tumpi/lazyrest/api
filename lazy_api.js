const restify = require('restify')
let errors = require('restify-errors');
const mongoose = require('./core/mongoose')
const {timeZone,port} = require('./core/config')
//const socketio = require('socket.io');
const corsMiddleware = require('restify-cors-middleware')

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['Authorization']
})


process.env.TZ = timeZone;

let server = restify.createServer({
    name: "lazyrestapi",
    version: "0.0.1"
});



server.pre(cors.preflight)
server.use(cors.actual)



server.use(restify.queryParser());
server.use(restify.bodyParser());


server.listen(process.env.port || port,(err)=>{
    if(err){
        console.log('error:',err.message);
        process.exit(1);
    }
    console.log('server listening on port',process.env.PORT || port);
    mongoose.configure(server);
});

process.on('uncaughtException',(err)=> {
    if(err){
        console.log("LazyRestApiError",err);
    }
});
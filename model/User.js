/**
 * Created by muharizals on 17/11/2017.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const dbUser = new Schema({
    name:String,
    email:{type:String,lowercase:true,trim:true},
    fb_token:String,
    phone_number:String
},{timestamps:Date});

dbUser.plugin(uniqueValidator);

module.exports = mongoose.model('User',dbUser);
/**
 * Created by muharizals on 17/11/2017.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const dbProject = new Schema({
    _author:{type:Schema.Types.ObjectId,ref:'User'},
    _project_name:String,
    _secret_key:{type:String,index:true,unique:true,sparse:true,trim:true,lowercase:true},
    _secure:{type:Boolean,default:false},
    _access_token:{type:String,index:true,unique:true,sparse:true}
},{timestamps:Date});

dbProject.pre('remove',function (next) {
    this.model('Collection').remove({ _project:this._id},next);
});

dbProject.plugin(uniqueValidator);

module.exports = mongoose.model('Project',dbProject);
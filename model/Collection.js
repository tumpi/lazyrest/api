/**
 * Created by muharizals on 17/11/2017.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')


const dbCollection = new Schema({
    _project:{type:Schema.Types.ObjectId,ref:'Project'},
    _author:{type:Schema.Types.ObjectId,ref:'User'},
    _name:{type: String, lowercase: true, trim: true},
    _data:Schema.Types.Mixed
},{timestamps:Date});

module.exports = mongoose.model('Collection',dbCollection);

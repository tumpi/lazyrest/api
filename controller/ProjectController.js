/**
 * Created by muharizals on 18/11/2017.
 */
const Model = require('../model');
let errors = require('restify-errors');
const Util = require('../util');

class ProjectController{
    create(req,res){
        if(!req.params.hasOwnProperty("_project_name")){
            return res.send(new errors.makeErrFromCode(400,"_project_name are required"));
        }
        let body = req.params;
        let key = `${Util.getFromName(body._project_name)}-${Util.generateSecretKey()}`;
        body._secret_key = key.toLowerCase();
        body._access_token = Util.genereateAccesToken();
        body._author = req.user.id;

        Model.Project.create(body,(err,result)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            res.send(result);
        });
    }

    get(req,res){
        let query = {
            _id:req.params.id,
            _author:req.user.id,
        };
        Model.Project.findOne(query,(err,result)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(Util.isEmpty(result)){
                return res.send(new errors.makeErrFromCode(404,"Project key not found"))
            }
            res.send(result);
        });
    }

    del(req,res){
        let query = {
            _id:req.params.id,
            _author:req.user.id,
        };
        Model.Project.findOneAndRemove(query,(err,result)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(Util.isEmpty(result)){
                return res.send(new errors.makeErrFromCode(404,"Project key not found"))
            }
            res.send(result);
        });
    }

    update(req,res){
        let query = {
            _id:req.params.id,
            _author:req.user.id,
        };
        Model.Project.findOneAndUpdate(query,req.params,{new:true},(err,result)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(Util.isEmpty(result)){
                return res.send(new errors.makeErrFromCode(404,"Project key not found"))
            }
            res.send(result);
        });
    }

    list(req,res){
        try{
            let options = Util.getOptions(req);
            let query = {
                _author:req.user.id,
            };

            Model.Project.find(query)
                .sort(options.sort)
                .skip(options.skip)
                .limit(options.limit)
                .lean()
                .exec((err,doc)=>{
                    if(err){
                        return res.send(new errors.makeErrFromCode(500,err.message))
                    }
                    let response = {
                        meta:{
                            current_page:options.page,
                            total_data:0,
                            limit:options.limit,
                            sort:options.sort,
                            offset:options.offset
                        },
                        data:[]
                    };
                    response.data = Object.assign([],doc);
                    Model.Project.count(query,(err,count)=>{
                        if(err){
                            return res.send(new errors.makeErrFromCode(500,err.message))
                        }
                        response.meta.total_data = count;
                        res.send(response);
                    });
                });

        }catch (err){
            res.send(err);
        }
    }
}

module.exports = new ProjectController();
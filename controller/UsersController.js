/**
 * Created by muharizals on 18/11/2017.
 */
const Model = require('../model');
let errors = require('restify-errors');
const Util = require('../util');

class UsersController{
    login(req,res){
        /**
         * email,id,phone_number
         */
        if(!req.params.hasOwnProperty("fb_token")){
            return res.send(new errors.makeErrFromCode(400,"fb_token required"));
        }
        Model.User.findOneAndUpdate({fb_token:req.body.fb_token},req.params)
            .lean()
            .exec((err,result)=>{
                if(err){
                    return res.send(new errors.makeErrFromCode(500,err.message))
                }
                if(Util.isEmpty(result)){
                    Model.User.create(req.body,(err,newUser)=>{
                        if(err){
                            return res.send(new errors.makeErrFromCode(500,err.message))
                        }
                        let response = newUser.toObject();
                        response.access_token = Util.getAccessToken({id:response._id});
                        res.send(response);
                    });
                }else{
                    result.access_token = Util.getAccessToken(({id:result._id}))
                    res.send(result);
                }
            });

    }
}

module.exports = new UsersController();